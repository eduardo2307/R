﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(R.Startup))]
namespace R
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
